import { NavigationActions } from 'react-navigation';
import AppNavigator from '../navigation/AppNavigation';

const initialState = AppNavigator.router.getStateForAction(NavigationActions.init());

const navReducer = (state = initialState, action) => {

    switch (action.type) {
      case 'Navigation/NAVIGATE':
        if (action.params && action.params.replaceRoute) {
          delete action.params.replaceRoute;
          if (state.routes.length > 1 && state.index > 0) {

            const index = state.index;

            state.routes.splice(index, 1);

            state.index = index - 1;

            return AppNavigator.router.getStateForAction(action, state);
          }
        }
      default:
        return AppNavigator.router.getStateForAction(action, state);
    }
}

export default navReducer;
