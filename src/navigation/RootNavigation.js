import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';
import AppNavigator from './AppNavigation';

class RootNavigator extends Component {
    render() {
        return (
            <AppNavigator navigation={
                addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav,
                })
            }/>
        )
    }
}

const mapStateToProps = state => {
    return {
        nav: state.nav
    }
}

export default connect(mapStateToProps)(RootNavigator);