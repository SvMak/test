import { StackNavigator } from 'react-navigation';
import DA from '../screens/DAScreen';
import DB from '../screens/DBScreen';

const DNavigator = StackNavigator({
    DA: {
      screen: DA,
      navigationOptions: {
        title: 'DA'
      }
    },
    DB: {
      screen: DB,
      navigationOptions: {
        title: 'DB'
      }
    },
},{
    headerMode: 'none'
});

export default DNavigator;
