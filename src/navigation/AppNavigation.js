import { StackNavigator } from 'react-navigation';
import A from '../screens/AScreen';
import B from '../screens/BScreen';
import C from '../screens/CScreen';
import DNavigator from './DNavigation';

const AppNavigator = StackNavigator({
    A: {
        screen: A,
        navigationOptions: {
            title: 'A'
        }
    },
    B: {
        screen: B,
        navigationOptions: {
            title: 'B'
        }
    },
    C: {
        screen: C,
        navigationOptions: {
            title: 'C'
        }
    },
    D: {
        screen: DNavigator
    },
});

export default AppNavigator;
