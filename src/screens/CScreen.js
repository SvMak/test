import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native';
import StateList from '../components/StateList';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

class C extends Component {

    handler = screen => {
        const action = NavigationActions.navigate({
            routeName: 'D',
            params: {
                replaceRoute: true
            },
            action: NavigationActions.navigate({ routeName: screen })
        });

        this.props.navigation.dispatch(action);
    }

    render() {
        return (
            <View style={{flex: 1, margin: 10}}>
                <Button buttonStyle={{margin: 5}} title='DA' onPress={ () => this.handler('DA')} />
                <Button buttonStyle={{margin: 5}} title='DB' onPress={ () => this.handler('DB')} />
                <StateList data={this.props.nav.routes} />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        nav: state.nav
    }
}

export default connect(mapStateToProps)(C);
