import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native';
import StateList from '../components/StateList';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

class A extends Component {

    handler = () => {
      const action = NavigationActions.navigate({
          routeName: 'B'
      });
      this.props.navigation.dispatch(action)
    }

    render() {
        return (
            <View style={{flex: 1, margin: 10}}>
                <Button title='B' onPress={ () => this.handler() } />
                <StateList data={this.props.nav.routes} />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        nav: state.nav
    }
}

export default connect(mapStateToProps)(A);
