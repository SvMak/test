import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native';
import StateList from '../components/StateList';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

class B extends Component {

    handler = () => {
      const action = NavigationActions.navigate({
          routeName: 'C'
      });
      this.props.navigation.dispatch(action)
    }

    render() {
        return (
            <View style={{flex: 1, margin: 10}}>
                <Button title='C' onPress={ () => this.handler() } />
                <StateList data={this.props.nav.routes} />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        nav: state.nav
    }
}

export default connect(mapStateToProps)(B);
