import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';

export default class StateList extends Component {
    
    render() {
        return (
            <View style={{flex: 1, margin: 20}}>
                <Text style={{textAlign: 'center'}}>Current navigation state</Text>
                <FlatList
                    data={this.props.data}
                    keyExtractor={this.keyExtractor}
                    renderItem={(item) => <Text>{item.item.routeName}</Text>}
                />
            </View>
        )
    }
}